include(bionic)

### update and upgrade
RUN apt update &&\
    apt upgrade --yes

### install some other packages
RUN apt install --yes wget

RUN wget https://releases.mattermost.com/5.20.1/mattermost-5.20.1-linux-amd64.tar.gz && \
    tar -xzf mattermost*.gz && \
    mv mattermost /opt && \
    mv /opt/mattermost/config /opt/mattermost/config.orig 
