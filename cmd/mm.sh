cmd_mm_help() {
    cat <<_EOF
    mm [...]
        Run mattermost commands inside the container.

_EOF
}

cmd_mm() {
    docker exec -it --user mattermost $CONTAINER env TERM=xterm bin/mattermost "$@"
}
