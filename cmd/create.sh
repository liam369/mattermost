cmd_create_help() {
    cat <<_EOF
    create
        Create the container '$CONTAINER'.

_EOF
}

rename_function cmd_create orig_cmd_create
cmd_create() {
    mkdir -p config data logs plugins client-plugins
    orig_cmd_create \
        --mount type=bind,src=/etc/localtime,dst=/etc/localtime,readonly \
        --mount type=bind,src=$(pwd)/config,dst=/opt/mattermost/config \
        --mount type=bind,src=$(pwd)/data,dst=/opt/mattermost/data \
        --mount type=bind,src=$(pwd)/logs,dst=/opt/mattermost/logs \
        --mount type=bind,src=$(pwd)/plugins,dst=/opt/mattermost/plugins \
        --mount type=bind,src=$(pwd)/client-plugins,dst=/opt/mattermost/client/plugins \
        --workdir /opt/mattermost \
        "$@"

    _fix_wsproxy_apache2_configuration
}

_fix_wsproxy_apache2_configuration() {
    # fix wsproxy apache2 configuration
    cat <<EOF > $CONTAINERS/wsproxy/sites-available/$DOMAIN.conf
<VirtualHost *:80>
        ServerName $DOMAIN
        ProxyPass /.well-known !
        Redirect permanent / https://$DOMAIN/
</VirtualHost>

<VirtualHost _default_:443>
        ServerName $DOMAIN
        ProxyPreserveHost On

        RewriteEngine On
        RewriteCond %{REQUEST_URI} /api/v[0-9]+/(users/)?websocket [NC,OR]
        RewriteCond %{HTTP:UPGRADE} ^WebSocket$ [NC,OR]
        RewriteCond %{HTTP:CONNECTION} ^Upgrade$ [NC]
        RewriteRule .* ws://$DOMAIN:8065%{REQUEST_URI} [P,QSA,L]

        <Location />
                Require all granted
                ProxyPass http://$DOMAIN:8065/
                ProxyPassReverse http://$DOMAIN:8065/
        </Location>

        ProxyRequests off
        SSLEngine on
        SSLCertificateFile      /etc/ssl/certs/ssl-cert-snakeoil.pem
        SSLCertificateKeyFile   /etc/ssl/private/ssl-cert-snakeoil.key
        #SSLCertificateChainFile /etc/ssl/certs/ssl-cert-snakeoil.pem
</VirtualHost>
EOF
    # reload the new configuration
    ds @wsproxy reload
}
