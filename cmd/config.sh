cmd_config_help() {
    cat <<_EOF
    config
        Run configuration scripts inside the container.

_EOF
}

cmd_config() {
    # run standard config scripts
    ds inject ubuntu-fixes.sh
    ds inject ssmtp.sh

    # create the database
    ds mariadb create

    # run mattermost config scripts
    ds inject mattermost.sh
}
