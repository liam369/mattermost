APP=mattermost

### Docker settings.
DOMAIN="mattermost.example.org"
IMAGE=$DOMAIN
CONTAINER=$DOMAIN

### Email needed for getting a letsencrypt ssl cert.
SSL_CERT_EMAIL=admin@example.org

### DB settings
DBHOST=mariadb
DBPORT=3306
DBNAME=${DOMAIN//./_}
DBUSER=${DOMAIN//./_}
DBPASS=${DOMAIN//./_}

### SMTP server for sending notifications. You can build an SMTP server
### as described here:
### https://gitlab.com/docker-scripts/postfix/blob/master/INSTALL.md
### Comment out if you don't have a SMTP server and want to use
### a gmail account (as described below).
#SMTP_SERVER=smtp.example.org
#SMTP_DOMAIN=$DOMAIN

### Gmail account for notifications. This will be used by ssmtp.
### You need to create an application specific password for your account:
### https://www.lifewire.com/get-a-password-to-access-gmail-by-pop-imap-2-1171882
#GMAIL_ADDRESS=btr.example.org@gmail.com
#GMAIL_PASSWD=
