#!/bin/bash -x

source /host/settings.sh

main() {
    create_user_and_group
    setup_systemd_service

    # initialize the config file, if it does not exist
    if [[ -f config/config.json ]]; then
        : # do nothing
    else
        cp -a config.orig/config.json config/
        setup_mattermost_config
    fi

    systemctl start mattermost.service
}

# create a user and group for mattermost
create_user_and_group() {
    useradd --system --user-group mattermost
    chown -R mattermost:mattermost /opt/mattermost
    chmod -R g+w /opt/mattermost
}

setup_systemd_service() {
    cat <<EOF > /lib/systemd/system/mattermost.service
[Unit]
Description=Mattermost
After=network.target
#After=mysql.service
#Requires=mysql.service

[Service]
Type=notify
ExecStart=/opt/mattermost/bin/mattermost
TimeoutStartSec=3600
Restart=always
RestartSec=10
WorkingDirectory=/opt/mattermost
User=mattermost
Group=mattermost
LimitNOFILE=49152

[Install]
#WantedBy=mysql.service
WantedBy=multi-user.target
EOF
    systemctl daemon-reload
    systemctl enable mattermost.service
}

config_set() {
    local cmd="/opt/mattermost/bin/mattermost config set"
    su mattermost -c "$cmd $@"
}

setup_mattermost_config() {
    # set site url
    config_set ServiceSettings.SiteURL "https://$DOMAIN/"

    # setup DB connection
    config_set SqlSettings.DriverName mysql
    config_set SqlSettings.DataSource "$DBUSER:$DBPASS@tcp($DBHOST:3306)/$DBNAME?charset=utf8mb4,utf8&readTimeout=30s&writeTimeout=30s"

    # setup SMTP for sending emails
    if [[ -n $SMTP_SERVER ]]; then
        config_set EmailSettings.SMTPServer "$SMTP_SERVER"
        config_set EmailSettings.SMTPPort 25
        config_set EmailSettings.ConnectionSecurity STARTTLS
        config_set EmailSettings.SMTPUsername ""
        config_set EmailSettings.SMTPPassword ""
        config_set EmailSettings.FeedbackName "$DOMAIN"
        config_set EmailSettings.FeedbackEmail "info@$SMTP_DOMAIN"
        config_set EmailSettings.ReplyToAddress "info@$SMTP_DOMAIN"
    elif [[ -n $GMAIL_ADDRESS ]]; then
        config_set EmailSettings.SMTPServer "smtp.gmail.com"
        config_set EmailSettings.SMTPPort 587
        config_set EmailSettings.ConnectionSecurity STARTTLS
        config_set EmailSettings.SMTPUsername "$GMAIL_ADDRESS"
        config_set EmailSettings.SMTPPassword "$GMAIL_PASSWD"
        config_set EmailSettings.FeedbackName "$DOMAIN"
        config_set EmailSettings.FeedbackEmail "$GMAIL_ADDRESS"
        config_set EmailSettings.ReplyToAddress "$GMAIL_ADDRESS"
    fi

    # set some other configuration options
    config_set EmailSettings.EnablePreviewModeBanner false
    config_set EmailSettings.RequireEmailVerification true
    config_set EmailSettings.SendEmailNotifications true
    config_set EmailSettings.EnableEmailBatching true

    config_set ServiceSettings.EnableMultifactorAuthentication true
    config_set ServiceSettings.EnableLinkPreviews true
    config_set ServiceSettings.EnableEmailInvitations true
    config_set ServiceSettings.EnableSVGs true
    config_set ServiceSettings.EnableLatex true

    config_set FileSettings.EnablePublicLink true

    config_set TeamSettings.EnableOpenServer true
}

# start the main function
main "$@"
